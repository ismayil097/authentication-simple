/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  View
} from 'react-native';
import firebase from 'firebase'
import { Header, Button, Spinner, Card } from './src/Components/Common'
import LoginForm from './src/Components/loginForm'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const config = {
  apiKey: "AIzaSyBzbDCwJJL8dqqwN74IkF8Yt0BrvRM6qBM",
  authDomain: "authentication-7ea3e.firebaseapp.com",
  databaseURL: "https://authentication-7ea3e.firebaseio.com",
  projectId: "authentication-7ea3e",
  storageBucket: "authentication-7ea3e.appspot.com",
  messagingSenderId: "890620903565"
};
class App extends Component {
  state = {
    loggedIn: null
  }

  componentWillMount() {
    if (!firebase.apps.length) {
      firebase.initializeApp(config);
    }

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({
          loggedIn: true
        });
      } else {
        this.setState({
          loggedIn: false
        });
      }
    })
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
        return (
          <Button onPress={()=>firebase.auth().signOut()}>
            Log Out
          </Button>
        );
      case false:
        return <LoginForm />
      default:
        return <Spinner size='large' />

    }
  }

  render() {
    return (
      <View>
        <Header headerText="Authentication" />
        {this.renderContent()}
      </View>
    );
  }
}

export default App;
